
## REDHAT CERTIFICATION
"Certifying with Red Hat can help individuals, teams, and organizations validate the knowledge needed to stay ahead of the technology curve. Whether you’re looking to take the next step in your career or trying to fill skills gaps in your company, we have certifications and exams that match your needs."

ROADMAP

![](../.img/CertificationRedHat.png)

Links para estudos, PDF, e muito mais:
- [ ] [Red Hat Certified System Administrator](https://www.redhat.com/en/services/certification/rhcsa)
- [ ] [Red Hat Certified Engineer](https://www.redhat.com/en/services/certification/rhce)